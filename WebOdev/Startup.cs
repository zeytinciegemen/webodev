﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(WebOdev.Startup))]
namespace WebOdev
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
